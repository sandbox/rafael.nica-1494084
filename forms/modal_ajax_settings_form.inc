<?php

/**
 * @file
 * This include file implements a sample the settings form for modal module.
 */

/**
 * Implements hook_form().
 * 
 * Modal Ajax form settings.
 * @param <array> $form_state The form state array.
 * @return <array> An array containing the form elements.
 */
function modal_ajax_settings_form($form, &$form_state) {
  $form = array();

  $form['css-class'] = array(
    '#type' => 'textfield',
    '#title' => t('Modal CSS class *'),
    '#description' => t('The css class name form all modals to activate properly the javascript functionalilty, used with modal id.'),
    '#default_value' => isset($form_state['values']['css-class']) ? $form_state['values']['css-class'] : variable_get('modal_ajax_css_class', MODAL_AJAX_CSS_CLASS), 
  );
  
  $form['close-class'] = array(
    '#type' => 'textfield',
    '#title' => t('Modal close wrapper CSS class *'),
    '#description' => t('The css class name form all modals to activate properly the javascript functionalilty to the modal close wrapper.'),
    '#default_value' => isset($form_state['values']['close-class']) ? $form_state['values']['close-class'] : variable_get('modal_ajax_css_close_class', MODAL_AJAX_CSS_CLOSE_CLASS), 
  );
  
  $form['url-theme-mapping'] = array(
    '#type' => 'textarea',
    '#title' => t('URL Theme Mapping'),
    '#description' => t(
      'Here you link the apropriate url address/wildcard to a apropriate theme into any hook_theme.<br />'
      . 'Use this notation: url||theme, and can input more items into a new line.<br />This field is not required, means to open any '
      . 'url not defined here in the "modal" template (default template).'
    ),
    '#default_value' => isset($form_state['values']['url-theme-mapping']) ? $form_state['values']['url-theme-mapping'] : variable_get('modal_ajax_url_theme_mapping', ''),
  );
  
  $form['anchor-class-auto-bind'] = array(
    '#type' => 'textarea',
    '#title' => t('Anchor/link classes to bind modal'),
    '#description' => t(
      'Here you link the link classes that will open their requests into the modal.<br />'
      . 'Put each class you want to bind in a new line (each link class by line).<br />'
      . 'This field is not required, means if null or empty, the links will have their normal behaviour.'
    ),
    '#default_value' => isset($form_state['values']['anchor-class-auto-bind']) ? $form_state['values']['anchor-class-auto-bind'] : variable_get('modal_ajax_anchor_class_auto_bind', ''),
  );
  
  $form['custom-css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use custom CSS for each modal'),
    '#description' => t('Flag it if you will use custom css for each modal such custom width, height...'),
    '#default_value' => isset($form_state['values']['custom-css']) ? $form_state['values']['custom-css'] : variable_get('modal_ajax_custom_css', MODAL_AJAX_CUSTOM_CSS),
    '#return_value' => 1,
    '#ajax' => array(
      'callback' => 'modal_ajax_settings_form_custom_css_js',
      'wrapper' => 'custom_css_wrapper',
      'method' => 'replace',
      'effect' => 'fade',
      'progress' => array('type' => 'none'),
    ),
  );
  
  $form['custom-css-wrapper'] = array(
    '#prefix' => '<div id="custom_css_wrapper">',
    '#suffix' => '</div>',
    '#tree' => FALSE,
  );
  
  $form['custom-css-wrapper']['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Modal Default CSS Settings'),
    '#collapsible' => TRUE,
  );
  
  $form['custom-css-wrapper']['fieldset']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Modal width'),
    '#description' => t('This sets a default width for all modals'),
    '#default_value' => isset($form_state['values']['width']) ? $form_state['values']['width'] : variable_get('modal_ajax_width', MODAL_AJAX_WIDTH),
  );
  
  
  $form['custom-css-wrapper']['fieldset']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Modal height'),
    '#description' => t('This sets a default height for all modals'),
    '#default_value' => isset($form_state['values']['height']) ? $form_state['values']['height'] : variable_get('modal_ajax_height', MODAL_AJAX_HEIGHT),
  );
    
  $form['advanced-fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Modal Ajax Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['advanced-fieldset']['history'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Modal History'),
    '#description' => t('Set this option, if you want to store the unclosed modals. In other words, when you close the new modal, the older unclosed modal loads again.'),
    '#default_value' => isset($form_state['values']['history']) ? $form_state['values']['history'] : variable_get('modal_ajax_enable_history', MODAL_AJAX_ENABLE_HISTORY),
    '#return_value' => 1,
  );
  
  $form['advanced-fieldset']['modal-link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Modal Links to open inside it'),
    '#description' => t('Flag it if you want to load any modal link inside it.'),
    '#default_value' => isset($form_state['values']['modal-link']) ? $form_state['values']['modal-link'] : variable_get('modal_ajax_enable_link', MODAL_AJAX_ENABLE_LINK),
    '#return_value' => 1,
  );
  
  $form['advanced-fieldset']['modal-render-submit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Render submitted forms in modal - Non-ajax forms'),
    '#description' => t('Flag it if you want to a form result previously submitted in the modal.'),
    '#default_value' => isset($form_state['values']['modal-render-submit']) ? $form_state['values']['modal-render-submit'] : variable_get('modal_ajax_render_submit', MODAL_AJAX_RENDER_SUBMIT),
    '#return_value' => 1,
  );
  
  $form['sample-hook-menu'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Sample Hook Menu'),
    '#description' => t('Flag it if you want to see the an functional example.'),
    '#default_value' => isset($form_state['values']['sample-hook-menu']) ? $form_state['values']['sample-hook-menu'] : variable_get('modal_ajax_enable_sample_hook_menu', MODAL_AJAX_ENABLE_SAMPLE_HOOK_MENU),
    '#return_value' => 1,
  );
  
  $form['save'] = array(
    '#type' => 'submit',
    '#name' => 'save',
    '#value' => t('Save'),
  );
  
  $form['reset'] = array(
    '#type' => 'submit',
    '#name' => 'reset',
    '#value' => t('Reset to default values'),
  );

  $form['#validate'][] = 'modal_ajax_settings_form_validate';
  $form['#submit'][] = 'modal_ajax_settings_form_submit';
  
  return $form;
}

/**
 * Implements hook_form_validate().
 * 
 * @param <array> $form An array containing the form elements.
 * @param <array> $form_state The form state array.
 */
function modal_ajax_settings_form_validate($form, &$form_state) {
  if (!$form_state['values']['custom-css']) {
    if (empty($form_state['values']['height'])) {
      form_set_error('height', 'Enter the height.');
    }
    elseif (!is_numeric($form_state['values']['height'])) {
      form_set_error('height', t('Enter only numeric values.'));
    }
    
    if (empty($form_state['values']['width'])) {
      form_set_error('width', 'Enter the width.');
    }
    if (!is_numeric($form_state['values']['width'])) {
      form_set_error('width', t('Enter only numeric values.'));
    }
  }
  
  if (empty($form_state['values']['css-class'])) {
    form_set_error('css-class', t('Modal CSS class is required.'));
  }
  
  if (empty($form_state['values']['close-class'])) {
    form_set_error('close-class', t('Modal close wrapper CSS class is required.'));
  }
}

/**
 * Implements hook_form_submit().
 * 
 * @param <array> $form An array containing the form elements.
 * @param <array> $form_state The form state array.
 */
function modal_ajax_settings_form_submit($form, &$form_state) {
  switch ($form_state['clicked_button']['#name']) {
    case 'save':
      variable_set('modal_ajax_custom_css', $form_state['values']['custom-css']);
      variable_set('modal_ajax_enable_sample_hook_menu', $form_state['values']['sample-hook-menu']);
      variable_set('modal_ajax_width', $form_state['values']['width']);
      variable_set('modal_ajax_height', $form_state['values']['height']);
      variable_set('modal_ajax_css_class', $form_state['values']['css-class']);
      variable_set('modal_ajax_css_close_class', $form_state['values']['close-class']);
      variable_set('modal_ajax_enable_history', $form_state['values']['history']);
      variable_set('modal_ajax_enable_link', $form_state['values']['modal-link']);
      variable_set('modal_ajax_render_submit', $form_state['values']['modal-render-submit']);
      variable_set('modal_ajax_url_theme_mapping', $form_state['values']['url-theme-mapping']);
      variable_set('modal_ajax_anchor_class_auto_bind', $form_state['values']['anchor-class-auto-bind']);
      break;
      
    case 'reset':
      variable_set('modal_ajax_custom_css', MODAL_AJAX_CUSTOM_CSS);
      variable_set('modal_ajax_enable_sample_hook_menu', MODAL_AJAX_ENABLE_SAMPLE_HOOK_MENU);
      variable_set('modal_ajax_width', MODAL_AJAX_WIDTH);
      variable_set('modal_ajax_height', MODAL_AJAX_HEIGHT);
      variable_set('modal_ajax_css_class', MODAL_AJAX_CSS_CLASS);
      variable_set('modal_ajax_css_close_class', MODAL_AJAX_CSS_CLOSE_CLASS);
      variable_set('modal_ajax_enable_history', MODAL_AJAX_ENABLE_HISTORY);
      variable_set('modal_ajax_enable_link', MODAL_AJAX_ENABLE_LINK);
      variable_set('modal_ajax_render_submit', MODAL_AJAX_RENDER_SUBMIT);
      variable_set('modal_ajax_url_theme_mapping', '');
      variable_set('modal_ajax_anchor_class_auto_bind', '');
      break;
      
    default:
      break;
  }
  
  // flushing caches to remake menu and admin menu
  menu_rebuild();
  drupal_set_message(t('Data saved successfuly.'));
  
  if ($form_state['values']['sample-hook-menu'] || ($form_state['clicked_button']['#name'] == 'reset')) {
    if ($form_state['values']['sample-hook-menu']) {
      drupal_set_message(t('<a href="#ajax-sample-form">Modal Example</a>'));
    }
    else {
      drupal_set_message(t('<a href="#node">Modal Example</a>'));
    }
  }
  
  // when refresh form, load database values
  unset($form_state['values']);
}

/**
 * Gets the form wrapper to render submit by ajax.
 * 
 * @param <array> $form An array containing the form elements.
 * @param <array> $form_state The form state array.
 * @return <array> Form wrapper to be rendered.
 */
function modal_ajax_settings_form_custom_css_js($form, $form_state) {
  if (($form_state['values']['custom-css'] == 1) || (variable_get('modal_ajax_custom_css', MODAL_AJAX_CUSTOM_CSS) == 1)) {
    $form['custom-css-wrapper']['fieldset']['height']['#disabled'] = TRUE;
    $form['custom-css-wrapper']['fieldset']['height']['#attributes'] = array(
      'disabled' => 'disabled',
    );
    
    $form['custom-css-wrapper']['fieldset']['width']['#disabled'] = TRUE;
    $form['custom-css-wrapper']['fieldset']['width']['#attributes'] = array(
      'disabled' => 'disabled',
    );
     
    $form['custom-css-wrapper']['fieldset']['#collapsed'] = TRUE; 
  }
  else {
    unset($form['custom-css-wrapper']['fieldset']['height']['#disabled']);
    unset($form['custom-css-wrapper']['fieldset']['height']['#attributes']);
    $form['custom-css-wrapper']['fieldset']['#collapsed'] = FALSE;
  }
  
  return $form['custom-css-wrapper']; 
}