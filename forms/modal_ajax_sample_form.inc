<?php

/**
 * @file
 * This include file implements a sample form with ajax submission 
 * to test on a modal as a ajax example.
 */

/**
 * Implements hook_form().
 * 
 * Sample form example.
 * @param <array> $form_state The form state array.
 * @return <array> An array containing the form elements.
 */
function modal_ajax_sample_form($form, &$form_state) {
  $form = array();
  
  $form['sample-form-wrapper'] = array(
    '#prefix' => '<div id="sample_form_wrapper">',
    '#suffix' => '</div>',
    '#tree' => FALSE,
  );
  
  $form['sample-form-wrapper']['messages'] = array(
    '#type' => 'markup',
    '#value' => modal_ajax_html_drupal_message(),
  );
  
  $form['sample-form-wrapper']['first-name'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#default_value' => isset($form_state['values']['first-name']) ? $form_state['values']['first-name'] : '',
  );
  
  $form['sample-form-wrapper']['last-name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#default_value' => isset($form_state['values']['last-name']) ? $form_state['values']['last-name'] : '',
  );
  
  $form['sample-form-wrapper']['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => isset($form_state['values']['message']) ? $form_state['values']['message'] : '',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#name' => 'submit',
    '#value' => t('Send'),
    '#ajax' => array(
      'callback' => 'modal_ajax_sample_form_js',
      'wrapper' => 'sample_form_wrapper',
      'method' => 'replace',
      'effect' => 'fade',
      'progress' => array('type' => 'none'),
    ),
  );
  
  $form['#validate'][] = 'modal_ajax_sample_form_validate';
  $form['#submit'][] = 'modal_ajax_sample_form_submit';
  
  return $form;
}

/**
 * Implements hook_form_validate().
 * 
 * @param <array> $form An array containing the form elements.
 * @param <array> $form_state The form state array.
 */
function modal_ajax_sample_form_validate($form, &$form_state) {
  $empty_fields = FALSE;
  
  if (empty($form_state['values']['first-name'])) {
    $empty_fields = TRUE;
    form_set_error('first-name', ' ');
  }
  elseif (!preg_match('/[a-z]/', $form_state['values']['first-name'])) {
    form_set_error('first-name', t('Enter only alphabetic characters.'));
  }
  
  if (empty($form_state['values']['last-name'])) {
    $empty_fields = TRUE;
    form_set_error('last-name', ' ');
  }
  elseif (!preg_match('/[a-z]/', $form_state['values']['last-name'])) {
    form_set_error('last-name', t('Enter only alphabetic characters.'));
  }
  
  if (empty($form_state['values']['message'])) {
    $empty_fields = TRUE;
    form_set_error('message', ' ');
  }
  
  if ($empty_fields) {
    drupal_set_message(t('Please input all required information.'), 'error');
  }
}

/**
 * Implements hook_form_submit().
 * 
 * @param <array> $form An array containing the form elements.
 * @param <array> $form_state The form state array.
 */
function modal_ajax_sample_form_submit($form, &$form_state) {
  drupal_set_message(t('Your data has been submitted.'));
}

/**
 * Gets the form wrapper to render submit by ajax.
 * 
 * @param <array> $form An array containing the form elements.
 * @param <array> $form_state The form state array.
 * @return <array> Form wrapper to be rendered.
 */
function modal_ajax_sample_form_js($form, $form_state) {
  return $form['sample-form-wrapper'];
}