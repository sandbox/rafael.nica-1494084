<?php

/**
 * @file
 * This include file implements coder validates, submits and menu executions
 * for modal.
 */

/**
 * Implementation of modal_ajax_execute_modal_content.
 * Gets the content to apropriate url/menu and renders it.
 */
function modal_ajax_execute_modal_content() {
  // retrieving language list from drupal
  $language_list = language_list();
  if (isset($language_list[$_POST['site_language']])) {
    $GLOBALS['language'] = $language_list[$_POST['site_language']];
  }

  $html_content = modal_ajax_get_rendered_html_content_per_modal_theme($_GET['q']);
  $css_diff = modal_ajax_array_recursive_diff(drupal_add_css(), $_SESSION['modal_ajax']['css']);
  $css_output = drupal_get_css($css_diff);
  
  $js_diff = modal_ajax_array_recursive_diff(drupal_add_js(), $_SESSION['modal_ajax']['js']);
  $js_output = drupal_get_js('header', $js_diff);
  $js_output .= drupal_get_js('footer', $js_diff);
  
  if ($js_diff['settings']) {
    $js_output .= '<script type="text/javascript">jQuery.extend(Drupal.settings, ' . 
      drupal_json_encode(call_user_func_array('array_merge_recursive', $js_diff['settings']['data'])) . 
      ');</script>' . PHP_EOL;
  }
  
  print $css_output;
  print $js_output;
  print $html_content;
  
  // executing footer as a normal menu renderization
  drupal_page_footer();
  exit;
}

/**
 * Implements hook_form_validate().
 * 
 * Fixes the submitted data.
 * @param <array> $form_state The form state array.
 */
function modal_ajax_fix_validate($form, &$form_state) {
  if (isset($form_state['post']) && is_array($form_state['post']) && count($form_state['post'])) {
    foreach ($form_state['post'] AS $key => $value) {
      if (!is_array($value)) {
        $form_state['post'][$key] = check_plain($value);
      }
    }
  }
  
  if (isset($form_state['values']) && is_array($form_state['values']) && count($form_state['values'])) {
    foreach ($form_state['values'] AS $key => $value) {
      if (!is_array($value)) {
        $form_state['values'][$key] = check_plain($value);
      }
    }
  }
}

/**
 * Gets the drupal message to render on appropriate wrapper
 * @return <string> html drupal message 
 */
function modal_ajax_html_drupal_message() {
  $errors = drupal_get_messages('error', TRUE);
  $errors = isset($errors['error']) ? $errors['error'] : FALSE;
  
  $status = drupal_get_messages('status', TRUE);
  $status = isset($status['status']) ? $status['status'] : FALSE;
  
  $html = '';
  
  if (is_array($status) && count($status)) {
    $html .= '<div class="messages @type@">';
    $html = str_replace('@type@', 'status', $html);
    $html .= '<ul>';
    
    foreach ($status AS $message) {
      $message = trim($message);
      
      if (!empty($message)) {
        $html .= '<li>' . $message . '</li>';
      }
    }
    
    $html .= '</ul></div>';
  }
  
  if (is_array($errors) && count($errors)) {
    $html .= '<div class="messages @type@">';
    $html = str_replace('@type@', 'error', $html);
    $html .= '<ul>';
    
    foreach ($errors AS $error) {
      $error = trim($error);
      
      if (!empty($error)) {
        $html .= '<li>' . $error . '</li>';
      }
    }
    
    $html .= '</ul></div>';
  }
  
  return $html;
}

/**
 * Implementation of modal_ajax_array_recursive_diff.
 * Return the difference of the passed arrays.
 * @param <array> $a_array1
 * @param <array> $a_array2
 * @return <array> the difference of the arrays.
 */
function modal_ajax_array_recursive_diff($a_array1, $a_array2) {
  $a_return = array();

  foreach ($a_array1 as $m_key => $m_value) {
    if (array_key_exists($m_key, $a_array2)) {
      if (is_array($m_value)) {
        $a_recursive_diff = modal_ajax_array_recursive_diff($m_value, $a_array2[$m_key]);
        if (count($a_recursive_diff)) { 
          $a_return[$m_key] = $a_recursive_diff;
        }
      }
      else {
        if ($m_value != $a_array2[$m_key]) {
          $a_return[$m_key] = $m_value;
        }
      }
    } 
    else {
      $a_return[$m_key] = $m_value;
    }
  }

  return $a_return;
}

/**
 * Gets the appropriate modal theme to passed url.
 * @param <string> $url Url to be matched with theme.
 * @return <string> Theme name.
 */
function modal_ajax_get_theme_per_url($url) {
  $url_theme_mapping = variable_get('modal_ajax_url_theme_mapping', '');
  $theme = 'modal';

  if (empty($url_theme_mapping)) {
    return $theme;    
  }
  else {
    $mapping = explode(PHP_EOL, $url_theme_mapping);
    foreach ($mapping AS $value) {
      $arr_mapping = explode("||", $value);
      if (is_array($arr_mapping)) {
        $arr_mapping[0] = trim($arr_mapping[0]);
        $arr_mapping[1] = trim($arr_mapping[1]);
        
        if (($url == $arr_mapping[0]) || drupal_match_path($url, $arr_mapping[0])) {
          $theme = modal_ajax_validate_theme($arr_mapping[1]) ? $arr_mapping[1] : $theme;
          break;
        }
      }
    }  
  }
  
  return $theme;
}

/**
 * Validates if theme exists in drupal.
 * @param <string> $theme theme name into any hook_theme.
 * @return <boolean>
 */
function modal_ajax_validate_theme($theme) {
  $modules = module_list();
  $arr_themes = array();
  
  foreach ($modules AS $module) {
    $result = module_invoke($module, 'theme');
    $arr_themes += is_array($result) ? $result : array();
  }
  
  return array_key_exists($theme, $arr_themes);
}

/**
 * Gets the rendered html content of the passed menu into its related theme.
 * @param <string> $url Menu url address.
 * @return <string> HTML rendered menu content.
 */
function modal_ajax_get_rendered_html_content_per_modal_theme($url) {
  $normal_path = drupal_get_normal_path($url, $GLOBALS['language']->language);
  $result = menu_execute_active_handler($normal_path, FALSE);
  $theme = modal_ajax_get_theme_per_url($url);
  
  $output = drupal_render($result);
  $output = (!$output) ? drupal_render_children($result) : $output;
  $output = trim($output);
  
  if (is_int($result)) {
    switch ($result) {
      case MENU_NOT_FOUND:
        $result = t('Not Found');
        break;
      case MENU_ACCESS_DENIED:
        $result = t('Forbidden');
        break;
      case MENU_SITE_OFFLINE:
        $result = t('Service unavailable');
        break;
    }
  }
  
  $theme_variables = array(
    'title' => drupal_get_title(),
    'messages' => modal_ajax_html_drupal_message(),
    'content' => (!$output) ? $result : $output,
  );
  
  return theme($theme, $theme_variables);
}