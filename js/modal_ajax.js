/**
 * Vars that will manipulate the modal callback
 */
var modal_callback = null;
var modal_id = null;

/**
 * Verifies if the form rendered in the modal can be submitted to it.
 * @returns <bool>
 */
function enableModalRenderSubmit() {
  return (((Drupal.settings.modal_ajax.submit_into_modal == 1) || (Drupal.settings.modal_ajax.submit_into_modal == true)) && (jQuery('.' + Drupal.settings.modal_ajax.css_class + ' form').length > 0));
}

/**
 * Enable modal links to open inside the modal
 */
function enableModalLinks() {
  jQuery('.' + Drupal.settings.modal_ajax.css_class + ' a').each(function(){
    if (jQuery(this).attr('href').indexOf('http') == -1) {
      jQuery(this).attr('href', '#' + jQuery(this).attr('href'));
      jQuery(this).attr('href', jQuery(this).attr('href').replace('//', '/'));
      
      if ((Drupal.settings.modal_ajax.basepath != "/") && (Drupal.settings.modal_ajax.basepath.length > 1)) {
    	  jQuery(this).attr('href', jQuery(this).attr('href').replace(Drupal.settings.modal_ajax.basepath, '/'));
      }
    }
  });
}

/**
 * Inserts the url hash into the history stack
 * @param <string> hash location's hash provided from url
 * @returns <bool> returns the flag if the hash was inserted into the history stack
 */
function pushHistory(hash) {
  // disable history according to admin form settings
  if (Drupal.settings.modal_ajax.enable_history == 0) {
    setCookie('modal_history', hash);
    return false;
  }

  var modal_ajax_cookie = getCookie('modal_history');

  if (!modal_ajax_cookie) {
    setCookie('modal_history', hash);
    return true;
  }
  else {
    // current settings
    var current_hook_menu = hash.trim();

    // last settings
    var last_hash_settings = modal_ajax_cookie.split('|');
    var last_hook_menu = last_hash_settings[(last_hash_settings.length - 1)].trim();

    // parsing url
    if (current_hook_menu.indexOf('?') != -1) {
      current_hook_menu = current_hook_menu.split('?')[0];
    }

    if (last_hook_menu.indexOf('?') != -1) {
      last_hook_menu = last_hook_menu.split('?')[0];
    }

    if (current_hook_menu != last_hook_menu) {
      setCookie('modal_history', modal_ajax_cookie + '|' + hash);

      return true;
    }
  }

  return false;
}

/**
 * Removes the current url hash from the history stack
 * @returns <string> returns the last url hash
 */
function popHistory() {
  var modal_ajax_cookie = getCookie('modal_history');
  var arr_cookie = modal_ajax_cookie.split('|'); 

  // unset cookie
  if (modal_ajax_cookie == arr_cookie) {
    var date = new Date();
    setCookie('modal_history', '');
  }
  else { // pop element from cookie
    arr_cookie.splice((arr_cookie.length - 1), 1);
    setCookie('modal_history', arr_cookie.join('|'));

    return arr_cookie[(arr_cookie.length - 1)];
  }

  return '';
}

/**
 * Implementation of modal callback
 * @param <string> func_id modal id css
 * @param <method> modal callback implementation
 * @returns <void>
 */
function modal_ajax_ready(func_id, func_callback) {
  modal_id = func_id;
  modal_callback = func_callback;
}

/**
 * Resizes the modal according to css class or admin parameters
 * @returns <void>
 */
function resizeModal() {
  var modal_class = Drupal.settings.modal_ajax.css_class;
  var close_class = Drupal.settings.modal_ajax.css_close_class;

  if (Drupal.settings.modal_ajax.custom_css == 0) {
    jQuery('.' + modal_class).css('width', Drupal.settings.modal_ajax.width + 'px');
    jQuery('.' + modal_class).css('height', Drupal.settings.modal_ajax.height + 'px');
  }

  if (jQuery('.' + close_class).length) {
    jQuery('.' + close_class).click(function(e){
      e.preventDefault();
      var new_hash = popHistory().replace('#', '').trim();

      if (new_hash == '') {
        jQuery('.' + modal_class).animate({
          opacity: 0
        }, 500, function(){
          jQuery('#modal-overlay').animate({
            opacity: 0.5,
            width:  0,
            height: 0,
            left: jQuery(document).width()/2,
            top: jQuery(document).height()/2
          }, 500, function(){
            jQuery('#modal-overlay').remove();
            jQuery('.' + modal_class).remove();
            location.hash = new_hash;
          });
        });
      }
      else {
        location.hash = new_hash;
      }
    });
  }
}

/**
 * Centers the modal on the screen.
 * @returns <void>
 */
function centerModal() {
  var modal_class = Drupal.settings.modal_ajax.css_class;
  var modal_width = jQuery('.' + modal_class).width();
  var document_width  = jQuery(window).width();
  var modal_height = jQuery('.' + modal_class).height();
  var screen_height  = jQuery(window).height();
  var left_modal = (document_width - modal_width)/2;
  var top_modal = (screen_height - modal_height)/2;

  jQuery('.' + modal_class).css('left', left_modal + 'px');
  jQuery('.' + modal_class).css('top', top_modal + 'px');
}

/**
 * Shows the modal on screen
 * @returns <void>
 */
function showModal() {
  resizeModal();
  if (jQuery('.' + Drupal.settings.modal_ajax.css_class).length > 1) {
    jQuery('.' + Drupal.settings.modal_ajax.css_class + ':first').remove();
    centerModal();
    jQuery('.' + Drupal.settings.modal_ajax.css_class).show();
    jQuery('.modal-loader').remove();
    if (modal_callback != null && jQuery('#' + modal_id).length) {
      modal_callback();	
    }

    Drupal.attachBehaviors();

    // enable modal links according to admin form settings.
    if (Drupal.settings.modal_ajax.enable_link == 1) {
      enableModalLinks();
    }
    
    if (enableModalRenderSubmit()) {
      jQuery('.' + Drupal.settings.modal_ajax.css_class + ' form').attr('action', location.href.replace((location.protocol + '//' + location.host), ""));
      jQuery('.' + Drupal.settings.modal_ajax.css_class + ' form').append('<input type="hidden" name="modal_action" value="' + location.hash.replace('#', '') + '" />');
    }
  }
  else {
    var overlay = jQuery('<div>');
    overlay.addClass('overlay');
    overlay.attr('id','modal-overlay');

    overlay.css({
      'filter': 'alpha(opacity=50)',
      'opacity': '0.5',
      '-moz-opacity': '0.5',
      '-webkit-opacity': '0.5',
      'background': '#000000',
      'width': jQuery(document).width() + 'px',
      'height': jQuery(document).height() + 'px',
      'position': 'absolute',
      'z-index': '2147483645',
      'left': 0,
      'top': 0
    });
	   
    jQuery('body').append(overlay);
    overlay.show();
    centerModal();
    jQuery('.' + Drupal.settings.modal_ajax.css_class).show();
    jQuery('.modal-loader').remove();
	    
    if (modal_callback != null && jQuery('#' + modal_id).length) {
      modal_callback();	
    }

    Drupal.attachBehaviors();

    // enable modal links according to admin form settings.
    if (Drupal.settings.modal_ajax.enable_link == 1) {
      enableModalLinks();
    }
	    
    if (enableModalRenderSubmit()) {
      jQuery('.' + Drupal.settings.modal_ajax.css_class + ' form').attr('action', location.href.replace((location.protocol + '//' + location.host), ""));
      jQuery('.' + Drupal.settings.modal_ajax.css_class + ' form').append('<input type="hidden" name="modal_action" value="' + location.hash.replace('#', '') + '" />');
    }
  }
}

/**
 * Onload event implementation
 */
jQuery(document).ready(function(){
  // cleaning history from page refresh
  var date = new Date();
  setCookie('modal_history', '');

  jQuery(window).hashchange(function(){
	if (modal_ajax_execute_hash == 0) {
	  showModal();
	  modal_ajax_execute_hash = 1;
	  return false;
	}
	
    if (location.hash.trim() != '') {
      var hook_menu = location.hash.replace('#', '').trim();

      if ((hook_menu == false) || (hook_menu == null) || (hook_menu == undefined) || (hook_menu == '')) {
        return false;
      }

      pushHistory(location.hash.replace('#', '').trim());
      jQuery('body').append('<div class="modal-loader">' + Drupal.t('Loading...') + '</div>');

      jQuery.ajax({
        url : Drupal.settings.modal_ajax.basepath + hook_menu,
        type: "POST",
        data: "modal_execution=true&site_language=" + Drupal.settings.modal_ajax.language,
        async: false,
        success : function(data){
          jQuery('body').append(data);
          if (jQuery('.' + Drupal.settings.modal_ajax.css_class).length) {
            showModal();  	
          }
        }
      });
    }
  });

  // activating modal behaviour in link classes
  if (Drupal.settings.modal_ajax.anchor_class_auto_bind.length) {
    for (var index in Drupal.settings.modal_ajax.anchor_class_auto_bind) {
      var auto_bind_class = Drupal.settings.modal_ajax.anchor_class_auto_bind[index].trim();
      if (auto_bind_class != '') {
        if (jQuery('.' + auto_bind_class).length) {
          jQuery('.' + auto_bind_class).each(function(){
            if (jQuery(this).attr('href').indexOf('http') == -1) {
              jQuery(this).attr('href', '#' + jQuery(this).attr('href'));
              jQuery(this).attr('href', jQuery(this).attr('href').replace('//', '/'));
        	      
              if ((Drupal.settings.modal_ajax.basepath != "/") && (Drupal.settings.modal_ajax.basepath.length > 1)) {
                jQuery(this).attr('href', jQuery(this).attr('href').replace(Drupal.settings.modal_ajax.basepath, '/'));
              }
            }
          });
        }
      }
    }
  }
  
  jQuery(window).hashchange();
});

/**
 * Window resize event implementation.
 */
jQuery(window).resize(function(){
  jQuery('#modal-overlay').css({
    'left': '0px',
    'top': '0px',
    'width': jQuery(document).width(),
    'height': jQuery(document).height(),
  });
  centerModal();
});

(function ($) {
  Drupal.behaviors.modal_ajax = {
    attach: function (context, settings) {
      if ($('div.error ul li').length) {    	
        $('div.error ul li').each(function(){
          var content = $(this).html().trim();
          if (content == '') {
            $(this).remove();
          }
        });
      }
    }
  };
})(jQuery);