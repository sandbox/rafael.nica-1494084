/**
 * Stores a value in the selected cookie
 * @param <string> c_name cookie name
 * @param <string> value cooike value
 * @param <date> exdays cookie expire time
 */
function setCookie(c_name,value,exdays)
{
  var exdate=new Date();
  exdate.setDate(exdate.getDate() + exdays);
  var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
  c_value += "; path=/;";
  document.cookie=c_name + "=" + c_value;
}

/**
 * Returns the selected cookie
 * @returns <string> cookie value
 */
function getCookie(c_name)
{
  var i, x, y, ARRcookies=document.cookie.split(";");

  for (i=0;i<ARRcookies.length;i++)
  {
    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
    y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
    x=x.replace(/^\s+|\s+$/g,"");
    if (x==c_name)
    {
      return unescape(y);
    }
  }

  return false;
}
