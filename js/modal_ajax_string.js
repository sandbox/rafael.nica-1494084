/**
 * String class overloads.
 */
String.prototype.trim = function() {
  return this.replace(/^\s+|\s+$/g, "");
}