<div id="modal" class="<?php print variable_get('modal_ajax_css_class', MODAL_AJAX_CSS_CLASS); ?>">
  <h2 class="modal-title"><?php print $title; ?></h2>
  <span class="<?php print variable_get('modal_ajax_css_close_class', MODAL_AJAX_CSS_CLOSE_CLASS); ?>"><?php print t('Close'); ?></span>
  <div class="modal-content"><?php print $messages . $content; ?></div>
</div>