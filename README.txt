
-- SUMMARY --

The Modal Ajax module activates the modal functionality to your site, rendering any menu 
into the modal according to admin page settings. Supports any new theme implamantation to
render any menu into your new modal template.
To enable any menu through the href element into <a>, just prepend an hash character before
the link: <a href="#my-menu">My menu</a>. Throuh javascript, just call location.hash statement
setting the menu value: <script type="text/javascript">location.hash="my-menu"</script>.

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/rafael.nica/1494084

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/1494084

-- REQUIREMENTS --

None.


-- INSTALLATION --

Like any drupal module, copy the modal_ajax folder into 
~your-drupal-installation-folder/sites/all/modules, loggin into the CMS system, goes to
admin/modules menu and enable Modal Ajax in the User Interface section.


-- CONFIGURATION --

To setting new options and administer the module functionality, access the 
admin/config/modal-ajax menu and save the data as you prefer to your site.


-- CUSTOMIZATION --

* To define a new modal template, just register it into your module hook_theme, with this
  arguments:
<?php
/**
 * Implements hook_theme().
 */
function my_module_name_theme() {
  $path_module = drupal_get_path('module', 'my_module_name');

  return array(
    'my_new_modal_template' => array(
      'variables' => array('title' => NULL, 'messages' => NULL, 'content' => NULL),
      'template' => 'my_new_modal_template',
      'path' => $path_module . '/templates',
    ),
  );
}
?>
The three variables are required and obligatory, to the rendering mechanism of module become
right.

* Define your basis html with this parameters, we will simulate some wrappers, the tags are to free
  use, you can define any tag, but you need to put at least the two basis wrappers and print the 
  variables. Example:
<div id="my-modal-id" class="<?php print variable_get('modal_ajax_css_class', MODAL_AJAX_CSS_CLASS); ?>">
  <span class="<?php print variable_get('modal_ajax_css_close_class', MODAL_AJAX_CSS_CLOSE_CLASS); ?>"><?php print t('Close'); ?></span>
  <?php print $title . $messages . $content; ?>
</div>

* Fush the site cache and enjoy it!

* You also can implement an event, when the modal be ready, setting some js implementation, 
if you consider it apropriate.

<script type="text/javascript">
modal_ajax_ready("your-modal-wrapper-id", function(){
  /**
   * here is an anonymous function
   * you implement your js that you should be implemented 
   * after the modal popup being loaded.
   */
});
</script>


-- CONTACT --
Current maintainers:
* Rafael Thiago Nica (rafael.nica) - http://drupal.org/user/1067692
* Vinicius Warto (vinicius.warto) - http://drupal.org/user/1871696